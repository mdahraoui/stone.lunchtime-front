import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLunchComponent } from './list-lunch.component';

describe('ListLunchComponent', () => {
  let component: ListLunchComponent;
  let fixture: ComponentFixture<ListLunchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLunchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLunchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
